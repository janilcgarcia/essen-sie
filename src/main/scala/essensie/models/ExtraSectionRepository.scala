package essensie.models

import doobie.*
import doobie.implicits.*
import cats.*
import cats.effect.*

object ExtraSectionRepository {
  private val baseSelect =
    fr"""SELECT id, name, relative_weight, position, recipe_id FROM extra_sections"""

  def getById(id: Long): ConnectionIO[ExtraSection] =
    sql"""$baseSelect WHERE id = $id"""
      .query[ExtraSection]
      .unique

  def getByRecipeId(id: Long): ConnectionIO[List[ExtraSection]] =
    sql"""$baseSelect WHERE recipe_id = $id ORDER BY position ASC"""
      .query[ExtraSection]
      .to[List]

  def create(section: ExtraSection): ConnectionIO[ExtraSection] =
    sql"""INSERT INTO extra_sections(name, relative_weight, position, recipe_id)
    VALUES (
      ${section.name}, ${section.relativeWeight}, ${section.index},
      ${section.recipeId}
    )"""
      .update
      .withUniqueGeneratedKeys[ExtraSection](
        "id", "name", "relative_weight", "position", "recipe_id")

  def update(section: ExtraSection): ConnectionIO[ExtraSection] =
    for {
      _ <- (
        sql"""UPDATE extra_sections SET
          name = ${section.name},
          relative_weight = ${section.relativeWeight},
          position = ${section.index},
          recipe_id = ${section.recipeId}
        WHERE id = ${section.id.get}
        """
      ).update.run

      section <- getById(section.id.get)
    } yield section

  def delete(section: ExtraSection): ConnectionIO[Int] =
    sql"""DELETE FROM extra_sections WHERE id = ${section.id.get}"""
      .update
      .run

  def deleteForRecipe(recipeId: Long): ConnectionIO[Int] =
    sql"""DELETE FROM extra_sections WHERE recipe_id = $recipeId"""
      .update
      .run
}
