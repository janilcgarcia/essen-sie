package essensie

import cats.*
import cats.arrow.FunctionK

import cats.effect.kernel.Fiber
import cats.effect.syntax.spawn.*
import cats.effect.{Async, ExitCode, IO, IOApp, Resource, Spawn, Sync}

import cats.syntax.applicative.*
import cats.syntax.flatMap.*
import cats.syntax.functor.*
import cats.syntax.traverse.*
import cats.syntax.semigroupk.*
import cats.syntax.monadError.*
import cats.syntax.option.*

import fs2.io.net.tls.{TLSContext, TLSParameters}

import org.http4s.dsl.io.*
import org.http4s.ember.server.EmberServerBuilder
import org.http4s.server.Server
import org.http4s.server.middleware.HSTS
import org.http4s.{Headers, HttpApp, HttpRoutes, Response, Status, Uri, headers}

import com.typesafe.config.{Config, ConfigFactory}

import essensie.config.*
import cats.effect.std.Dispatcher

import org.http4s.server.Router
import doobie.util.transactor.Transactor
import cats.data.Kleisli
import org.http4s.Request
import cats.data.OptionT
import doobie.util.transactor
import org.http4s.ContextRequest
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger
import essensie.services.PasswordHasher
import essensie.services.CookieSigning
import org.http4s.server.staticcontent.WebjarServiceBuilder
import org.http4s.server.staticcontent.WebjarService
import org.http4s.StaticFile
import io.taig.babel.NonEmptyTranslations
import essensie.i18n.Labels
import io.taig.babel.Loader
import io.taig.babel.Locales
import io.taig.babel.Decoder
import io.taig.babel.circe.*
import io.taig.babel.Encoder

import org.http4s.headers.`Accept-Language`
import org.http4s.headers.`Content-Type`

import java.util.{Locale as JavaLocale}
import scala.jdk.CollectionConverters.*
import org.http4s.MediaType

object EssenSieServer extends IOApp {
  given Logger[IO] = Slf4jLogger.getLogger

  private val index = HttpRoutes[IO] {
    case GET -> Root =>
      StaticFile.fromResource[IO]("/index.html")

    case GET -> Root / "index.css" =>
      StaticFile.fromResource[IO]("/index.css")

    case _ => OptionT.none
  }

  private val alwaysIndex = HttpRoutes[IO] { req =>
    StaticFile.fromResource[IO]("/index.html")
  }

  def app(
    config: AppConfig,
    transactor: Transactor[IO],
    i18n: NonEmptyTranslations[Labels]
  ): HttpApp[IO] = {
    val hasher = PasswordHasher()
    val sign = new CookieSigning(config.security.secretKey.getBytes())
    val manager = new services.SessionManager(sign, "session")

    val auth = routes.Auth[IO](transactor, hasher, manager)
    val recipes = routes.RecipesRoutes[IO](transactor)
    val collections = routes.CollectionRoutes[IO](transactor)

    val i18nRoute = HttpRoutes.of[IO] {
      case request @ GET -> Root / "i18n.json" =>
        val locale = request
          .headers
          .get[`Accept-Language`]
          .map(_.values.head)
          .flatMap { value =>
            JavaLocale.LanguageRange
              .parse(value.toString)
              .asScala
              .sortWith(_.getWeight > _.getWeight)
              .map(language => JavaLocale.forLanguageTag(language.getRange))
              .flatMap(io.taig.babel.Locale.fromJavaLocale)
              .find(locales.contains)
          }
          .getOrElse(Locales.en)

        Ok(printer.print(Encoder[Labels].encode(i18n(locale))))
          .map(_.withContentType(`Content-Type`(MediaType.application.json)))
    }
    val apiRoutes = (
      auth.loginRoutes <+>
        auth.middleware(
          auth.queryRoutes <+>
            recipes.routes <+>
            collections.routes
        )
    )
    
    val app =
      Router(
        "/" -> (
          index <+>
            i18nRoute <+>
            alwaysIndex
        ),
        "/api" -> apiRoutes,
        "/assets" -> WebjarServiceBuilder[IO].toRoutes
      )

    ErrorLoggingMiddleware[IO](app.orNotFound)
  }

  val locales = Set(Locales.en, Locales.pt_BR)

  def loadTranslations[F[_]: Sync]: F[NonEmptyTranslations[Labels]] =
    Loader
      .default[F]
      .load("langs", locales)
      .map(Decoder[Labels].decodeAll)
      .rethrow
      .flatMap(_.withFallback(Locales.en).liftTo[F](
        new IllegalStateException("Translation for en missing"))
      )

  /** Make a TLS context for the config defined for the HTTPS server
    *
    * @param config
    *   Configuration for the HTTPS key store.
    */
  private def makeTlsContext[F[_]: Async](
    config: KeyStoreConfig
  ): F[TLSContext[F]] = TLSContext
    .Builder
    .forAsync[F]
    .fromKeyStoreFile(
      config.path.toPath(),
      config.password.toCharArray(),
      config.password.toCharArray()
    )

  /** Make TLS parameters for the HTTPS server. Currently only apply a
    * restriction to the TLS version.
    *
    * @param config
    *   Configuration for the HTTPS key store.
    */
  private def makeTlsParameters(config: KeyStoreConfig): TLSParameters =
    TLSParameters(protocols = Some(List("TLSv1.3")))

  /** App that redirects the user to the HTTPS version of the app. When HTTPS is enable,
    * so is HSTS and the user is not allowed to access the HTTP host.
    *
    * @param config
    *   Configuration for the server, must contain information for both HTTP and
    *   HTTPS endpoints
    */
  def redirectHttpsApp[F[_]: Async](config: ServerConfig): HttpApp[F] =
    HttpApp { req =>
      val host = req.headers.get[headers.Host]

      val redirectHost = host
        .map(h => Uri.RegName(h.host))
        .orElse(req.serverAddr.map(Uri.Host.fromIpAddress))
        .getOrElse(Uri.RegName("localhost"))

      Response[F](
        status = Status.PermanentRedirect,
        headers = Headers(
          headers.Location(
            Uri(
              scheme = Some(Uri.Scheme.https),
              authority = Some(
                Uri.Authority(
                  host = redirectHost,
                  port = Option(config.https.port.value).filter(_ != 443)
                )
              )
            )
          )
        )
      ).pure
    }

  /** Grabs a resource defining a server and run it forever.
    *
    * @param resource
    *   Server to start
    */
  private def startServer[F[_]: Async: Logger: Spawn](
    resource: Resource[F, Server]
  ): F[Fiber[F, Throwable, Unit]] =
    resource
      .use { server =>
        for {
          _ <- Logger[F].info(s"Serving at: $server")
          _ <- Sync[F].never
        } yield ()
      }
      .start

  /** Build HTTP servers with Ember. If you have HTTPS enabled, it will build
    * two servers, otherwise only the one HTTP.
    *
    * @param config
    *   ServerConfig with the configuration for both servers
    * @param app
    *   Base app that will run on the server. In case HTTPS is enabled the
    *   produced HTTP server will not server this app, but instead redirect to
    *   HTTPS.
    */
  def buildServers[F[_]: Async](
    config: ServerConfig,
    app: HttpApp[F]
  ): F[List[Resource[F, Server]]] = {
    if (config.useHttps) {
      val hstsApp = HSTS(app)

      // HTTP server
      val http = EmberServerBuilder
        .default[F]
        .withHttpApp(redirectHttpsApp(config))
        .withHost(config.http.bind)
        .withPort(config.http.port)
        .withShutdownTimeout(config.shutdownTimeout)

      // HTTPS server
      val https = EmberServerBuilder
        .default[F]
        .withHttpApp(hstsApp)
        .withHost(config.https.bind)
        .withPort(config.https.port)
        .withShutdownTimeout(config.shutdownTimeout)

      // build the server list
      for {
        context <- makeTlsContext[F](config.https.keystore)
        params = makeTlsParameters(config.https.keystore)
      } yield List(http.build, https.withTLS(context, params).build)
    } else {
      List(
        EmberServerBuilder
          .default[F]
          .withHttpApp(app)
          .withHost(config.http.bind)
          .withPort(config.http.port)
          .withShutdownTimeout(config.shutdownTimeout)
          .build
      ).pure
    }
  }

  def makeTransactor[F[_]: Async](config: DatabaseConfig): Transactor[F] =
    Transactor.fromDriverManager[F](
      driver = config.driver,
      url = config.url,
      config.user,
      config.password
    )

  override def run(args: List[String]): IO[ExitCode] = {
    for {
      i18n <- loadTranslations[IO]
      config <- AppConfig.loadConfig[IO]
      _ <- Logger[IO].info(s"Config: $config")
      servers <- buildServers(
        config.server,
        app(
          config,
          makeTransactor(config.database),
          i18n
        ))
      fibers <- servers.map(startServer).sequence
      _ <- fibers.map(_.join).sequence
    } yield ExitCode.Success
  }
}
