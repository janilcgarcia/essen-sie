package essensie.config

class ConfigLoadingException(message: String, cause: Throwable = null)
    extends Exception(message, cause)
