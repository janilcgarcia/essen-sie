package essensie.config

import pureconfig.ConfigReader
import pureconfig.generic.derivation.default.*

/** PostgreSQL connection configuration.
  *
  * @param driver
  *   driver used in the database
  * @param password
  *   password for user in database server
  * @param user
  *   user in the database server, defaults to postgres
  * @param migrationsTable
  *   table where information about the migrations executed is stored
  * @param migrationsLocations
  *   locations to look for migrations
  */
case class DatabaseConfig(
  val driver: String,
  val url: String,
  val user: String,
  val password: String,

  val migrationsTable: String,
  val migrationsLocations: List[String]
) derives ConfigReader
