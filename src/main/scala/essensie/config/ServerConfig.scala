package essensie.config

import pureconfig.ConfigReader
import pureconfig.generic.derivation.default.*

import scala.concurrent.duration.FiniteDuration

/** Configuration related to the server setup.
  * @param useHttps
  *   whether or not the HttpsServer should be enabled.
  * @param http
  *   configuration for the HTTP server
  * @param https
  *   configuration for the HTTPS server
  */
case class ServerConfig(
  val useHttps: Boolean,
  val http: HttpServerConfig,
  val https: HttpsServerConfig,
  val shutdownTimeout: FiniteDuration
) derives ConfigReader
