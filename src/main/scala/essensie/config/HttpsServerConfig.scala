package essensie.config

import com.comcast.ip4s.{Host, Port}
import pureconfig.ConfigReader
import pureconfig.generic.derivation.default.*

/** Config for the HTTPS server.
  *
  * @param bind
  *   address where the HTTPS server should run.
  * @param port
  *   port where the HTTPS server should run.
  * @param hsts
  *   if HSTS should be enabled. SHOULD be true unless testing some specific
  *   HSTS aspect.
  * @param keystore
  *   KeyStore holding the server private key
  */
case class HttpsServerConfig(
  val bind: Host,
  val port: Port,
  val hsts: Boolean,
  val keystore: KeyStoreConfig
) derives ConfigReader
