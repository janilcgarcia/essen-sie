package essensie.services

import cats.Alternative
import cats.syntax.option.*
import java.time.Instant
import cats.effect.Sync
import org.bouncycastle.crypto.digests.Blake2bDigest

import java.nio.charset.StandardCharsets.UTF_8
import java.util.Base64
import java.time.Clock
import fs2.text.base64
import scala.util.Try
import org.bouncycastle.util.Arrays

class CookieSigning(
  private val key: Array[Byte]
) {
  private val b64Encoder = Base64.getUrlEncoder
  private val b64Decoder = Base64.getUrlDecoder

  private def b64Encode(bs: Array[Byte]): String = {
    val result = b64Encoder.encodeToString(bs)
    val firstEq = result.indexOf('=')

    if (firstEq < 0)
      result
    else
      result.take(firstEq)
  }

  private def b64Decode(encoded: String): Try[Array[Byte]] =
    Try(b64Decoder.decode(encoded))

  private def signContents(contents: String): Array[Byte] = {
    val b2 = new Blake2bDigest(key)
    val bytes = contents.getBytes(UTF_8)
    val output = new Array[Byte](b2.getDigestSize())

    b2.update(bytes, 0, bytes.length)
    b2.doFinal(output, 0)
    output
  }

  def sign(contents: String): String = {
    val b64Contents = b64Encode(contents.getBytes(UTF_8))
    val sig = b64Encode(signContents(contents))

    s"$b64Contents.$sig"
  }

  def verify(signed: String): Option[String] = {
    val parts = signed.split("\\.")

    for {
      (b64contents, sig) <- Option.when(parts.length == 2)(parts(0) -> parts(1))
      contents <- b64Decode(b64contents).flatMap { bytes =>
        Try(new String(bytes))
      }.toOption
      sigBytes <- b64Decode(sig).toOption
      sigBytes1 = signContents(contents)
      _ <- Alternative[Option].guard(
        Arrays.constantTimeAreEqual(sigBytes, sigBytes1)
      )
    } yield contents
  }
}
