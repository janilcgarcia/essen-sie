package essensie.services

import org.http4s.Response
import org.http4s.ResponseCookie
import essensie.services.CookieSigning
import org.http4s.SameSite
import org.http4s.Request

class SessionManager(val sign: CookieSigning, val cookieName: String) {
  def destroySession[F[_]](response: Response[F]): Response[F] =
    response.removeCookie(cookieName)

  def setSession[F[_]](response: Response[F], session: Session): Response[F] =
    response.addCookie(
      ResponseCookie(
        cookieName,
        session.toCookie(sign),

        path = Some("/"),
        sameSite = Some(SameSite.Strict),
        httpOnly = true
      )
    )

  def getSession[F[_]](request: Request[F]): Option[Session] =
    for {
      cookie <- request.cookies.find(_.name == cookieName)
      session <- Session.fromCookie(sign, cookie.content)
    } yield session
}

object SessionManager {
  object Extensions {
    extension [F[_]](response: Response[F]) {
      def destroySession(using manager: SessionManager) =
        manager.destroySession(response)

      def setSession(session: Session)(using manager: SessionManager) =
        manager.setSession(response, session)
    }

    extension [F[_]](request: Request[F]) {
      def getSession(using manager: SessionManager) =
        manager.getSession(request)
    }
  }
}
