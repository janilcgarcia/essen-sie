package essensie.cli

import cats.effect.{IOApp, IO, Sync, ExitCode}

import doobie.syntax.all.*

import java.io.Console

import essensie.util.makeTransactor

import essensie.config.AppConfig
import essensie.config.DatabaseConfig
import essensie.models.UserRepository
import essensie.models.User
import essensie.services.PasswordHasher

class Console[F[_]: Sync] private (private val console: java.io.Console) {
  def readPassword(prompt: Option[String]): F[String] =
    Sync[F].delay(prompt match {
      case Some(prompt) =>
        new String(console.readPassword(prompt))

      case None =>
        new String(console.readPassword())
    })

  def readLine(prompt: Option[String]): F[String] =
    Sync[F].delay(prompt match {
      case Some(prompt) =>
        console.readLine(prompt)

      case None =>
        console.readLine()
    })
}

object Console {
  def apply[F[_]: Sync]: F[Console[F]] = Sync[F].delay(
    new Console(System.console)
  )
}

object createUser extends IOApp {
  override def run(args: List[String]): IO[ExitCode] = {
    val hasher = PasswordHasher()

    for {
      console <- Console[IO]
      config <- AppConfig.loadConfigOn[IO, DatabaseConfig](Some("database"))
      xa = makeTransactor[IO](config)

      email <- console.readLine(Some("E-mail: "))
      fullName <- console.readLine(Some("Full Name: ")).map(s =>
        Option.unless(s.trim.isEmpty)(s)
      )
      password <- console.readPassword(Some("Password: "))

      user <- (
        UserRepository
          .insert(User.make(email, fullName, password, hasher))
          .transact(xa)
      )

      _ <- IO.println(s"User created, id: ${user.id.get}")
    } yield ExitCode.Success
  }
}
