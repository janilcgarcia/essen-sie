create table users(
  id serial primary key,
  email varchar(255) not null unique,
  full_name varchar(150),
  public_name varchar(50),
  password_hash varchar(250) not null,
  created_at timestamp with time zone not null default current_timestamp,
  updated_at timestamp with time zone not null default current_timestamp,
  password_last_set_at timestamp with time zone
    not null default current_timestamp
);

create or replace function users_update_timestamps()
returns trigger as $$
begin
  if row(NEW.*) is distinct from row(OLD.*) then
    NEW.updated_at = current_timestamp;
  else
    return OLD;
  end if;

  if NEW.password_hash != OLD.password_hash then
    NEW.password_last_set_at = current_timestamp;
  end if;

  return NEW;
end;
$$ language 'plpgsql';

create trigger users_update_timestamps
  before update on users
  for each row execute function users_update_timestamps();
