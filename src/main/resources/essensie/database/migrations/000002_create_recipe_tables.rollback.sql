drop table if exists extra_ingredients;
drop table if exists extra_sections;
drop table if exists main_ingredients;
drop table if exists recipes;
drop table if exists recipe_collections;
