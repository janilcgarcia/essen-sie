create table recipe_collections(
  id serial primary key,
  name varchar(90) not null,
  user_id integer not null references users(id) on delete cascade
);

create table recipes(
  id serial primary key,
  name varchar(250) not null,
  flour_weight numeric(4, 1) not null,
  user_id integer not null references users(id) on delete cascade,
  collection_id integer not null references recipe_collections(id)
  on delete cascade
);

create table main_ingredients(
  id serial primary key,
  name varchar(250) not null,
  flour_weight numeric(4, 1) not null,
  unique_id varchar(50) not null,
  position integer not null,
  recipe_id integer not null references recipes(id) on delete cascade
);

create table extra_sections(
  id serial primary key,
  name varchar(250) not null,
  relative_weight numeric(4, 1) not null,
  recipe_id integer not null references recipes(id) on delete cascade
);

create table extra_ingredients(
  id serial primary key,
  unique_id varchar(50) not null,
  flour_weight numeric(4, 1) not null,
  position integer not null,
  section_id integer not null references extra_sections(id) on delete cascade
);
