package essensie.services

import org.scalacheck.Gen
import org.scalacheck.Arbitrary.arbitrary
import org.scalacheck.Prop.*

import java.nio.charset.StandardCharsets.UTF_8
import java.util.Base64

import weaver.SimpleIOSuite
import weaver.scalacheck.Checkers

object CookieSigningSuite extends SimpleIOSuite with Checkers {
  val defaultKey = "01020304050608".getBytes(UTF_8)
  val signer = new CookieSigning(defaultKey)

  test("A correctly signed cookie can be verified") {
    forall { (content: String) =>
      expect(signer.verify(signer.sign(content))
        .map(_ == content)
        .getOrElse(false))
    }
  }

  test("A correctly signed cookie has two parts") {
    forall { (content: String) =>
      expect.eql(signer.sign(content).split("\\.").size, 2)
    }
  }

  test("Changing a bit on the signature invalidates cookie") {
    forall { (content: String) =>
      val cookie = signer.sign(content)
      val Array(c, sig) = cookie.split("\\.")
      val sigBytes = Base64.getUrlDecoder.decode(sig).toSeq

      forall(Gen.choose(0, sigBytes.size - 1)) { pos =>
        val tampered = sigBytes.updated(pos, (sigBytes(pos) ^ 0x1).toByte)

        val tamperedInvalid = signer.verify(
          c + "." + Base64.getUrlEncoder.encodeToString(tampered.toArray)
        ).isEmpty

        val normalValid = signer.verify(
          c + "." + Base64.getUrlEncoder.encodeToString(sigBytes.toArray)
        ).isDefined

        expect(tamperedInvalid) && expect(normalValid)
      }
    }
  }

  test("Changing the content invalidates cookie") {
    val nonEmptyString = Gen.nonEmptyListOf(Gen.asciiPrintableChar)
      .map(_.mkString)

    forall(nonEmptyString) { content =>
      val cookie = signer.sign(content)
      val Array(orig, sig) = cookie.split("\\.")
      val variantA = content + "a"
      val variantAB64 = signer.sign(variantA).split("\\.")(0)
      val variantB = content.updated(0, (content(0).toInt + 1).toChar)
      val variantBB64 = signer.sign(variantA).split("\\.")(0)

      val ok = signer.verify(variantAB64 + "." + sig).isEmpty &&
      signer.verify(variantBB64 + "." + sig).isEmpty

      expect(ok)
    }
  }
}
