package essensie

import cats.syntax.functor.*
import cats.effect.{Resource, IO}

import weaver.{IOSuite, TestName}

import doobie.Transactor

import java.sql.Savepoint
import doobie.util.transactor.Strategy
import weaver.Expectations

trait DatabaseTestSupport { self: IOSuite =>
  def transactorFromRes(res: self.Res): Transactor[IO]

  private def rollbackTransactor(
    transactor: Transactor[IO]
  ): Resource[IO, Transactor[IO]] = {
    transactor.connect(transactor.kernel).flatMap { conn =>
      val acquire = IO {
        val auto = conn.getAutoCommit()
        conn.setAutoCommit(false)
        (conn.setSavepoint(), auto)
      }

      def release(savepoint: Savepoint, auto: Boolean) = IO {
        conn.rollback(savepoint)
        conn.setAutoCommit(auto)
      }

      Resource.make(acquire)(release.tupled).as(conn)
    }.map { conn =>
      val xa = Transactor.fromConnection(conn)
      Transactor.strategy.set(xa, Strategy.void)
    }
  }

  def txTest(name: TestName): PartiallyAppliedTxTest =
    new PartiallyAppliedTxTest(name)

  class PartiallyAppliedTxTest(name: TestName) {
    def apply(body: Transactor[IO] => IO[Expectations]) =
      self.test(name)(
        transactorFromRes.andThen(rollbackTransactor(_).use(body))
      )

    def apply(body: (Transactor[IO], Res) => IO[Expectations]) =
      self.test(name)(res =>
        transactorFromRes.andThen(rollbackTransactor(_).use(body(_, res)))(res)
      )
  }
}

