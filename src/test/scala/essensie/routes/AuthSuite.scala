package essensie.routes

import cats.effect.IO
import cats.syntax.semigroupk.*
import doobie.*
import doobie.syntax.all.*
import essensie.DatabaseTestSupport
import org.http4s.HttpRoutes

import essensie.TestPasswordHasher
import essensie.services.{CookieSigning, SessionManager, Session}
import essensie.models.UserRepository
import essensie.models.User
import org.http4s.Request
import org.http4s.Method
import org.http4s.Entity
import org.http4s.syntax.literals.*

import io.circe.*
import io.circe.syntax.*

import org.http4s.circe.CirceEntityCodec.*
import org.http4s.Status
import org.http4s.EntityEncoder
import essensie.apimodels.LoginInput
import essensie.apimodels.UserType


object AuthSuite extends weaver.SimpleIOSuite with DatabaseTestSupport {
  val hasher = TestPasswordHasher()

  override def transactorFromRes(res: Res) = Transactor.fromDriverManager(
    "org.postgresql.Driver",
    "jdbc:postgresql://localhost/essensie_test",
    "postgres",
    "postgres"
  )

  def makeAuth(xa: Transactor[IO]): Auth[IO] =
    Auth(
      xa,
      hasher,
      new SessionManager(
        new CookieSigning("ab".getBytes),
        "session"
      )
    )

  txTest("Log in sets session to the right user") { xa =>
    val auth = makeAuth(xa)

    for {
      _ <- (
        UserRepository
          .insert(User.make("abc@localhost", None, "abc", hasher))
          .transact(xa)
      )

      response <- auth.loginRoutes.orNotFound.run(Request(
        method = Method.POST,
        uri = uri"/login",
        entity = EntityEncoder[IO, LoginInput].toEntity(LoginInput(
          "abc@localhost",
          "abc"
        ))
      ))

      decoded <- response.as[UserType].attempt
    } yield (
      expect(response.status == Status.Ok) &&
        expect(response.cookies.exists(c =>
          c.name == "session" && !c.content.isBlank
        ))
    )
  }

  txTest("Log in fails with wrong user name or password") { xa =>
    val auth = makeAuth(xa)

    for {
      _ <- (
        UserRepository
          .insert(User.make("abc@localhost", None, "abc", hasher))
          .transact(xa)
      )

      responseA <- auth.loginRoutes.orNotFound.run(Request(
        method = Method.POST,
        uri = uri"/login",
        entity = EntityEncoder[IO, LoginInput].toEntity(LoginInput(
          "abc@localhost",
          "abcd"
        ))
      ))

      responseB <- auth.loginRoutes.orNotFound.run(Request(
        method = Method.POST,
        uri = uri"/login",
        entity = EntityEncoder[IO, LoginInput].toEntity(LoginInput(
          "abc@google.com",
          "abc"
        ))
      ))
    } yield (
      expect(responseA.status == Status.Forbidden)
        && expect(responseB.status == Status.Forbidden)
    )
  }

  txTest("Log in changes active user") { xa =>
    val auth = makeAuth(xa)
    val routes = auth.loginRoutes.combineK(
      auth.middleware(auth.queryRoutes)
    ).orNotFound

    val getUser = Request[IO](
      method = Method.GET,
      uri = uri"/users/active"
    )

    for {
      _ <- (
        UserRepository
          .insert(User.make("abc@localhost", None, "abc", hasher))
          .transact(xa)
      )
      responseA <- routes.run(getUser)
      responseB <- routes.run(Request(
        method = Method.POST,
        uri = uri"/login",
        entity = EntityEncoder[IO, LoginInput].toEntity(LoginInput(
          "abc@localhost",
          "abc"
        ))
      ))
      responseC <- routes.run(
        responseB.cookies.foldLeft(getUser) { (req, cookie) =>
          req.addCookie(cookie.name, cookie.content)
        }
      )
      user <- responseC.as[UserType]
    } yield (
      expect(!responseA.status.isSuccess)
        && expect(responseB.status == Status.Ok)
        && expect(responseC.status == Status.Ok)
        && expect(user.email == "abc@localhost")
    )
  }
}
