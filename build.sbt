import Dependencies._

ThisBuild / scalaVersion := "3.1.2"

ThisBuild / organization := "br.dev.safelydysfunctional"

lazy val root = project.in(file("."))
  .settings(
    name := "essen-sie",
    version := "0.1.0-SNAPSHOT",

    scalacOptions ++= Seq(
      "-deprecation", "-feature", "-source:future",
      "-Ykind-projector"
    ),

    scalaJSProjects := Seq(client),
    Assets / pipelineStages := Seq(scalaJSPipeline),
    Compile / compile := (Compile / compile).dependsOn(scalaJSPipeline).value,
    Assets / WebKeys.packagePrefix := "public/",
    Runtime / managedClasspath += (Assets / packageBin).value,

    libraryDependencies ++= allDeps ++ http4s ++ weaver,

    testFrameworks += new TestFramework("weaver.framework.CatsEffect"),

    Compile / mainClass := Some("essensie.EssenSieServer"),

    packageName := "essensie",

    dockerBaseImage := "openjdk:11"
  )
  .dependsOn(sharedJvm)
  .enablePlugins(WebScalaJSBundlerPlugin, JavaAppPackaging, DockerPlugin)

lazy val client = project.in(file("ui"))
  .settings(
    name := "essen-sie-ui",
    version := "0.1.0-SNAPSHOT",

    scalaJSUseMainModuleInitializer := true,

    libraryDependencies ++=
      Seq(
        "org.scala-js" %%% "scalajs-dom" % "2.0.0",
        "io.github.cquiroz" %%% "scala-java-time" % "2.3.0",
        "com.raquo" %%% "laminar" % "0.14.2",
        "com.raquo" %%% "waypoint" % "0.5.0",
        "com.raquo" %%% "domtypes" % "0.15.1",
        "io.laminext" %%% "fetch-circe" % "0.14.3",
        "org.typelevel" %%% "cats-effect" % "3.3.0"
      ),

    Compile / fullOptJS / scalaJSLinkerConfig ~= {
      _.withCheckIR(false)
    },
    Test / fullOptJS / scalaJSLinkerConfig ~= {
      _.withCheckIR(false)
    },

    webpackEmitSourceMaps := false,

    Compile / npmDependencies ++= Seq(
      "bootstrap" -> "^5.1.3",
      "@popperjs/core" -> "^2.11.5"
    ),

    Compile / npmDevDependencies ++= Seq(
      "webpack-merge" -> "5.7.3",
      "style-loader" -> "2.0.0",
      "css-loader" -> "5.0.1",
    ),

    Compile / webpackConfigFile :=
      Some(baseDirectory.value / "webpack.config.js"),

    useYarn := true

    // Compile / fastLinkJS / jsMappings :=
    //   (Compile / fastLinkJS / jsMappings)
    //     .value
    //     .map { case (file, _) =>
    //       file -> file.getName()
    //     },
    // Compile / fullLinkJS / jsMappings :=
    //   (Compile / fullLinkJS / jsMappings)
    //     .value
    //     .map { case (file, _) =>
    //       file -> file.getName()
    //     }
  )
  .dependsOn(sharedJs)
  .enablePlugins(ScalaJSBundlerPlugin)

lazy val shared =
  crossProject(JSPlatform, JVMPlatform)
    .crossType(CrossType.Pure)
    .in(file("shared"))
    .settings(name := "essen-sie-shared")
    .settings(Dependencies.circeDeps)
    .settings(Dependencies.babel)

lazy val sharedJs = shared.js
lazy val sharedJvm = shared.jvm
