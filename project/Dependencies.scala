import sbt._
import Keys._
import org.portablescala.sbtplatformdeps.PlatformDepsPlugin.autoImport._
import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport._
import sbt.librarymanagement.DependencyBuilders

object Dependencies {
  private object versions {
    val http4s = "1.0.0-M31"
    val doobie = "1.0.0-RC2"
    val circe = "0.14.1"
    val weaver = "0.7.11"
    val babel = "0.5.0"
  }

  lazy val http4s = Seq("http4s-dsl", "http4s-ember-server", "http4s-circe")
    .map("org.http4s" %% _ % versions.http4s)

  lazy val doobie = Seq("doobie-core", "doobie-postgres")
    .map(
      "org.tpolecat" %% _ % versions.doobie
    )

  lazy val typesafeConfig = "com.typesafe" % "config" % "1.4.1"

  lazy val slf4jImpl = "org.slf4j" % "slf4j-jdk14" % "1.7.32"

  lazy val circeDeps = Def.settings(
    libraryDependencies ++=
      Seq("circe-core", "circe-generic", "circe-parser")
        .map("io.circe" %%% _ % versions.circe)
  )
  
  lazy val babel = Def.settings(
    libraryDependencies ++= Seq("babel-circe", "babel-loader", "babel-generic")
      .map("io.taig" %%% _ % versions.babel)
  )

  lazy val pgsql = "org.postgresql" % "postgresql" % "42.3.1"
  lazy val bouncycastle = "org.bouncycastle" % "bcprov-jdk15on" % "1.70"
  lazy val pureconfig = "com.github.pureconfig" %% "pureconfig-core" % "0.17.1"

  lazy val weaver = Seq("weaver-cats", "weaver-scalacheck")
    .map("com.disneystreaming" %% _ % versions.weaver)

  lazy val allDeps =
    Seq(
      typesafeConfig,
      pgsql,
      bouncycastle,
      slf4jImpl,
      pureconfig
    ) ++ doobie
}
