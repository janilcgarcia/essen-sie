package essensie.apimodels

import java.time.OffsetDateTime
import io.circe.Codec

case class RecipeShort(
  val id: Long,
  val name: String,
  val collectionId: Option[Long]
) derives Codec.AsObject
