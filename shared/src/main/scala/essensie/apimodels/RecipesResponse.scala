package essensie.apimodels

import io.circe.Codec

case class RecipesResponse(
  val freeRecipes: List[RecipeShort],
  val collections: List[RecipeCollection]
) derives Codec.AsObject
