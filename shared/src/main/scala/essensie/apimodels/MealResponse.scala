package essensie.apimodels

import essensie.models.MealType

import java.time.OffsetDateTime
import essensie.apimodels.MealInput
import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec

case class MealResponse(
  val id: Long,
  val description: String,
  val extraNotes: Option[String],
  val hungerAtBeginning: Option[Int],
  val hungerAtEnd: Option[Int],
  val mealType: MealType,
  val photos: Seq[MealPhotoResponse],
  val ateAt: OffsetDateTime
) {
  def toInput: MealInput = MealInput(
    Some(id),
    description,
    extraNotes,
    hungerAtBeginning,
    hungerAtEnd,
    mealType,
    photos.map(_.id),
    Some(ateAt)
  )
}

object MealResponse {
  implicit val mealResponseCirceCodec: Codec[MealResponse] = deriveCodec
}
