package essensie.apimodels

import io.circe.Codec

case class SuccessfulDelete(val id: Long) derives Codec.AsObject
