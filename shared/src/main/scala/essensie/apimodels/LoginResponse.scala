package essensie.apimodels

import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec

case class LoginRequest(val username: String, val password: String)

object LoginRequest {
  implicit val loginRequestCodec: Codec[LoginRequest] = deriveCodec
}

case class LoginResponse(val accessToken: String, val refreshToken: String)

object LoginResponse {
  implicit val loginResponseCodec: Codec[LoginResponse] = deriveCodec
}
