package essensie.apimodels

import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec

case class NewCollectionInput(
  val name: String
)

object NewCollectionInput {
  given newCollectionInputCodec: Codec[NewCollectionInput] = deriveCodec
}
