package essensie.apimodels

import io.circe.Codec

case class RecipeCollection(
  val id: Long,
  val name: String,
  val recipes: List[RecipeShort]
) derives Codec.AsObject
