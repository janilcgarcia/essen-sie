package essensie.apimodels

import io.circe.Codec

case class MainSection(
  val name: String,
  val ingredients: Seq[Ingredient]
) derives Codec.AsObject
