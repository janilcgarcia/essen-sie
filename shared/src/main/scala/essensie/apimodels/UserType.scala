package essensie.apimodels

import io.circe.Codec

case class UserType(
  val id: Long,
  val email: String,
  val fullName: Option[String]
) derives Codec.AsObject
