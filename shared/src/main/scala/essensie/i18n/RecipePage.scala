package essensie.i18n

import io.taig.babel.StringFormat1

case class RecipePage(
  val defaultTitle: String,
  val mainSection: String,
  val defaultExtraSectionTitle: String,
  val flourWeight: String,
  val baseFlourPercentage: String,
  val remaining: String,
  val allOf: StringFormat1,
  val forms: RecipePage.Forms
)

object RecipePage {
  case class Forms(
    val addSection: String,
    val ingredientName: String,
    val percentage: String,
    val chooseOne: String
  )
}
