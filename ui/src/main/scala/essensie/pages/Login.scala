package essensie.pages

import com.raquo.laminar.api.L.*
import io.laminext.fetch.circe.*

import essensie.apimodels
import apimodels.UserType
import essensie.i18n.Labels
import com.raquo.waypoint.Router
import essensie.PageInfo
import essensie.RecipesPageInfo

class Login(
  val labels: Labels,
  val activeUser: Var[Option[UserType]],
  val router: Router[PageInfo]
) {
  private val email = Var("")
  private val password = Var("")

  private val loginActions = new EventBus[Unit]
  private val login = loginActions
    .events
    .withCurrentValueOf(email.signal, password.signal)
    .flatMap { (email, password) =>
      Fetch.post(
        "/api/login",
        body = apimodels.LoginInput(
          email, password
        )
      )
        .decodeOkay[apimodels.UserType]
        .map(_.data)
    }
    .recoverToTry
    .map(_.toOption)

  val root = div(
    cls := ("container", "d-flex", "justify-content-center", "mt-4", "pt-4"),

    div(
      cls := ("login-form", "w-50"),

      h2(cls := "text-center", "Breadpan"),
      h3(cls := "text-center", "Login"),

      child.maybe <-- login.startWithNone.splitOne(_.isEmpty) { (isEmpty, _, _) =>
        if (isEmpty)
          None
        else
          Some(p(cls := "text-danger", labels.loginPage.invalidUser))
      },

      form(
        cls := "form",

        div(
          cls := "mb-3",
          label(
            cls := "form-label", forId := "login-form-email",
            labels.loginPage.email
          ),

          input(
            typ := "email", cls := "form-control", idAttr := "login-form-email",
            controlled(
              value <-- email,
              onInput.mapToValue --> email
            ),

            cls.toggle("is-invalid") <-- EventStream.merge(
              login.map(_.isEmpty),
              email.signal.changes.mapToStrict(false)
            )
          )
        ),

        div(
          cls := "mb-3",

          label(
            cls := "form-label", forId := "login-form-password",
            labels.loginPage.password
          ),

          input(
            typ := "password",
            cls := "form-control",
            idAttr := "login-form-password",
            controlled(
              value <-- password,
              onInput.mapToValue --> password
            ),

            cls.toggle("is-invalid") <-- EventStream.merge(
              login.map(_.isEmpty),
              password.signal.changes.mapToStrict(false)
            )
          )
        ),

        div(
          cls := "d-grid",

          button(
            cls := ("btn", "btn-primary"), typ := "submit",
            "Login"
          )
        ),

        onSubmit.preventDefault.mapToStrict(()) --> loginActions
      )
    ),

    login --> activeUser,
    login.collect {
      case Some(_) => RecipesPageInfo
    } --> router.pushState
  )
}
