package essensie.pages

import org.scalajs.dom.window

import com.raquo.laminar.api.L.*
import io.circe.syntax.*
import io.laminext.fetch.circe.*

import essensie.i18n.Labels
import essensie.apimodels.RecipeDetails
import essensie.components.ExtraSectionCard
import essensie.components.MainSectionCard
import essensie.apimodels.MainSection
import essensie.components.EditableText
import essensie.ui.TextInput
import essensie.components.RemainingSection
import com.raquo.waypoint.Router
import essensie.PageInfo
import essensie.RecipesPageInfo
import scala.util.Success
import scala.util.Failure

class RecipeEditor(
  val labels: Labels,
  val initial: Signal[RecipeDetails],
  val router: Router[PageInfo]
) {
  private val flourWeight = Var(500.0)
  private val currentSections = Var(Vector.empty[ExtraSectionCard])

  private val sectionDeletes = currentSections.signal.flatMap { sections =>
    EventStream.mergeSeq(
      sections.zipWithIndex.map { (section, index) =>
        section.onDelete.mapToStrict(index)
      }
    )
  }

  private val mainSection = MainSectionCard(labels)(
    flourWeight.signal,
    MainSection(labels.recipePage.mainSection, Seq())
  )

  private val recipeName = EditableText.h1(labels.recipePage.defaultTitle)()

  val recipe =
    initial
      .combineWithFn(
        recipeName.value,
        flourWeight.signal,
        mainSection.sectionSignal,
        currentSections.signal.flatMap(sections =>
          Signal.combineSeq(
            sections.map(_.section)
          )
        )
      ) { (base, name, weight, mainSection, extraSections) =>
        RecipeDetails(
          base.id,
          base.collectionId,
          name,
          weight,
          mainSection,
          extraSections
        )
      }

  private val saveRecipeBus = EventBus[Unit]()
  private val saveRecipe =
    saveRecipeBus
      .events
      .sample(recipe)
      .flatMap { recipe =>
        Fetch
          .post(
            "/api/recipes",
            recipe.asJson
          )
          .decodeOkay[RecipeDetails]
          .recoverToTry
      }

  val root = div(
    cls := "container",
    child <-- recipeName.root,

    TextInput.Input(Some(labels.recipePage.flourWeight))(
      "number",
      Seq(
        defaultValue <-- flourWeight.signal.map("%.1f".format(_)),

        inContext { input =>
          onInput.mapToValue.map { mass =>
            if (mass.isEmpty)
              0.0
            else
              mass.toDouble
          } --> flourWeight
        },

        minAttr := "1",
        stepAttr := "0.5"
      )
    ).amend(
      cls.toggle("col-6", "offset-6") := true
    ),

    mainSection.root.amend(cls.toggle("mb-3") := true),

    button(
      cls := ("btn", "btn-primary", "mb-3"),

      labels.recipePage.forms.addSection,

      onClick --> currentSections.updater { (sections, _) =>
        sections :+ ExtraSectionCard(labels)(
          mainSection.sectionSignal.map(_.ingredients),
          flourWeight.signal
        )
      }
    ),

    children <-- currentSections.signal.map(_.map(_.root)),

    RemainingSection(
      labels,
      mainSection.sectionSignal.map(_.ingredients),
      currentSections.signal.flatMap { sections =>
        Signal.combineSeq(sections.map(_.section))
      },
      flourWeight.signal
    ),

    div(
      cls := ("d-flex", "justify-content-end", "my-2"),

      a(
        cls := ("btn", "btn-danger"),
        href := router.relativeUrlForPage(RecipesPageInfo),
        onClick.preventDefault.mapToStrict(RecipesPageInfo) --> router.pushState,
        "Cancel"
      ),

      button(
        cls := ("btn", "btn-primary", "ms-2"),
        onClick.mapToStrict(()) --> saveRecipeBus,
        "Save"
      )
    ),

    sectionDeletes --> currentSections.updater[Int] { (sections, index) =>
      sections.take(index) ++ sections.drop(index + 1)
    },

    saveRecipe --> {
      case Success(_) =>
        router.pushState(RecipesPageInfo)
      case Failure(ex) =>
        ex.printStackTrace()
        window.alert("Error")
    },

    initial --> { recipe =>
      flourWeight.set(recipe.baseFlourWeight)
      recipeName.set(recipe.name)

      mainSection.set(recipe.mainSection)
      currentSections.set(
        recipe.extraSections.map { extra =>
          ExtraSectionCard(labels)(
            mainSection.sectionSignal.map(_.ingredients),
            flourWeight.signal,
            extra
          )
        }.toVector
      )
    }
  )
}
