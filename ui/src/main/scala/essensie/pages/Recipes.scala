package essensie.pages

import com.raquo.laminar.api.L.*
import io.laminext.fetch.circe.*
import io.circe.syntax.*

import essensie.i18n.Labels
import com.raquo.waypoint.Router
import io.laminext.fetch.Fetch
import essensie.apimodels.{UserType, RecipesResponse}
import essensie.PageInfo
import essensie.components.{Util, NewCollectionForm}
import essensie.apimodels.RecipeShort
import essensie.apimodels.RecipeCollection
import essensie.apimodels.NewCollectionInput
import essensie.apimodels.SuccessfulDelete
import com.raquo.laminar.modifiers.ChildInserter
import essensie.components.Component
import essensie.RecipeEditorPageInfo
import essensie.NewRecipePageInfo

class Recipes(
  val labels: Labels,
  val activeUser: Signal[UserType],
  val router: Router[PageInfo]
) {
  import Recipes.*

  private val recipes =
    Fetch.get("/api/recipes")
      .decodeOkay[RecipesResponse]
      .map(_.data)

  private val freeRecipes =
    recipes
      .map(response => (_: List[RecipeShort]) => response.freeRecipes)
      .foldLeft(List.empty[RecipeShort])((list, op) => op(list))

  private val noCollectionId = Util.uniqueId("no-collection")

  private def renderNoCollectionItem(signal: Signal[RecipeShort]) =
    li(
      cls := "list-group-item",

      child.text <-- signal.map(_.name)
    )

  private def noCollectionSection(
    exists: Boolean,
    recipes: Signal[List[RecipeShort]]
  ) = {
    Option.when(exists)(
      div(
        cls := "accordion",

        div(
          cls := "accordion-item",

          h2(
            cls := "accordion-header",
            button(
              cls := ("accordion-button", "collapsed"),
              dataAttr("bs-toggle") := "collapse",
              dataAttr("bs-target") := s"#$noCollectionId",

              "No collection"
            )
          ),

          div(
            cls := ("accordion-collapse", "collapse"),
            idAttr := noCollectionId,
            div(
              cls := "accordion-body",

              ul(
                cls := ("list-group", "list-group-flush"),

                children <-- recipes.split(_.id) { (key, _, signal) =>
                  renderNoCollectionItem(signal)
                }
              )
            )
          )
        )
      )
    )
  }

  private val isCollectionFormOpen = Var(false)
  private val collectionForm = NewCollectionForm(labels, true)
  private val deleteCollectionBus = new EventBus[Long]
  private val deleteCollection =
    deleteCollectionBus
      .events
      .flatMap { id =>
        Fetch
          .delete(s"/api/collections/${id}")
          .decodeOkay[SuccessfulDelete]
          .map(_.data)
      }

  private val createCollection = collectionForm
    .onSubmit
    .flatMap { name =>
      Fetch
        .post("/api/collections/create", NewCollectionInput(name).asJson)
        .decodeOkay[RecipeCollection]
    }

  private val collections =
    EventStream.merge(
      recipes
        .map(response => (_: List[RecipeCollection]) => response.collections),

      createCollection
        .map(coll => xs => coll.data :: xs),

      deleteCollection.map { response => xs =>
        xs.filter(_.id != response.id)
      }
    ).foldLeft(List.empty[RecipeCollection])((list, op) => op(list))

  val root = div(
    cls := "container",

    h1("Recipes"),

    div(
      cls := ("d-flex", "justify-content-end"),
      button(
        cls := ("btn", "btn-primary", "me-2"),

        disabled <-- isCollectionFormOpen,

        "Add Collection",

        onClick.mapToStrict(true) --> isCollectionFormOpen
      )
    ),

    div(
      cls.toggle("d-none") <-- isCollectionFormOpen.signal.map(open => !open),
      cls := ("card", "my-3"),

      div(
        cls := "card-body",
        collectionForm
      ),

      collectionForm.onCancel.mapToStrict(false) --> isCollectionFormOpen,
      collectionForm.onCancel.mapToStrict(()) --> collectionForm.clear,

      createCollection.mapToStrict(false) --> isCollectionFormOpen,
      createCollection.mapToStrict(()) --> collectionForm.clear
    ),

    CollectionAccordion(
      Val(None),
      Val("Outras Receitas"),
      freeRecipes,
      labels,
      router,
      false
    ),

    children <-- collections.split(_.id) { (key, _, collection) =>
      val accordion = CollectionAccordion(
        collection.map(_.id).map(Option.apply),
        collection.map(_.name),
        collection.map(_.recipes),
        labels,
        router,
        true
      )

      accordion.root.amend(
        accordion.onDelete --> deleteCollectionBus
      )
    },

    child.maybe <-- recipes
      .splitOne(r => r.freeRecipes.isEmpty && r.collections.isEmpty) {
        (bothEmpty, _, _) =>
        if (bothEmpty)
          Some(h3("No recipes"))
        else
          None
      }
  )
}

object Recipes {
  private sealed trait ListOperation[+T]

  private object ListOperation {
    case class Set[+T](val list: List[T]) extends ListOperation[T]
    case class Append[+T](val value: T) extends ListOperation[T]
    case class Remove(val position: Int) extends ListOperation[Nothing]
  }

  class CollectionAccordion(
    val id: Signal[Option[Long]],
    val name: Signal[String],
    val recipes: Signal[Seq[RecipeShort]],
    val labels: Labels,
    val router: Router[PageInfo],
    val deletable: Boolean
  ) extends Component[Div] {
    private val accordionId = Util.uniqueId("collection-accordion")

    private def renderRecipes(recipes: Signal[Seq[RecipeShort]]): Element =
      div(
        cls := ("list-group", "mb-2"),

        children <--
          recipes.split(_.id)((_, _, recipe) =>
            a(
              cls := ("list-group-item", "list-group-item-action"),
              href <-- recipe.map(r =>
                router.absoluteUrlForPage(RecipeEditorPageInfo(r.id))
              ),
              inContext(
                _
                  .events(onClick.preventDefault)
                  .sample(recipe)
                  .map(r => RecipeEditorPageInfo(r.id))
                  --> router.pushState
              ),
              child.text <-- recipe.map(_.name)
            )
          )
      )

    private val deleteBus = new EventBus[Long]

    val onDelete = deleteBus.events

    val root = div(
      cls := ("accordion", "mt-2"),

      div(
        cls := "accordion-item",

        h2(
          cls := "accordion-header",
          button(
            cls := ("accordion-button", "collapsed"),

            dataAttr("bs-toggle") := "collapse",
            dataAttr("bs-target") := s"#$accordionId",

            child.text <-- name
          )
        ),

        div(
          cls := ("accordion-collapse", "collapse", "p-3"),
          idAttr := accordionId,

          child <-- recipes.splitOne(_.isEmpty) {
            (isEmpty, _, recipes) =>

            if (isEmpty) {
              "No recipes in this collection"
            } else {
              renderRecipes(recipes)
            }
          },

          div(
            cls := ("d-flex", "justify-content-end"),

            child.maybe <-- Val(Option.when(deletable)(
              button(
                cls := ("btn", "btn-danger", "me-1"),
                typ := "button",
                labels.basicButtons.delete + " Coleção",

                inContext(ctx =>
                  ctx.events(onClick).sample(id).map(_.get) --> deleteBus
                )
              )
            )),

            button(
              cls := ("btn", "btn-primary"),
              "Adicionar Receita",

              inContext { button =>
                val clickEventsWithId =
                  button
                    .events(onClick.preventDefault)
                    .sample(id)

                clickEventsWithId.map(NewRecipePageInfo.apply) -->
                  router.pushState
              }
            )
          )
        )
      )
    )
  }
}
