package essensie.components

import com.raquo.laminar.api.L
import L._

import essensie.apimodels.MainSection
import essensie.i18n.Labels

class MainSectionCard private (
  val root: Div,
  val labels: Labels,

  private val name: EditableText,
  private val ingredientList: IngredientList
) extends Component[Div] {
  val sectionSignal: Signal[MainSection] =
    name.value.combineWith(ingredientList.values).map { (name, list) =>
      MainSection(name, list)
    }

  def set(section: MainSection): Unit = {
    name.set(section.name)
    ingredientList.set(section.ingredients)
  }
}

object MainSectionCard {
  def apply(
    i18n: Labels
  )(
    flour: Signal[Double],
    initial: MainSection = MainSection(i18n.recipePage.mainSection, Seq.empty)
  ): MainSectionCard = {
    val name = EditableText(
      s => h4(
        cls := ("card-title", "mb-0")
      ).amend(s*),
      initial.name
    )

    val insertEvents = new EventBus[Unit]
    val ingredientList = IngredientList(
      i18n, insertEvents.events, initial.ingredients, flour
    )

    new MainSectionCard(
      div(
        cls := ("card", "mt-3"),

        div(
          cls := "card-body",

          div(
            cls := ("d-flex", "justify-content-between", "align-items-center", "mb-3"),

            child <-- name.root,

            button(
              cls := ("btn", "btn-primary"),
              i18n.basicButtons.add,
              onClick.mapToStrict(()) --> insertEvents.writer
            )
          ),
          ingredientList.root
        )
      ),
      i18n,

      name,
      ingredientList
    )
  }
}
