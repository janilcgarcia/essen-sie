package essensie.components

import com.raquo.laminar.api.L.*
import essensie.i18n.Labels
import com.raquo.laminar.nodes.ReactiveElement

trait Component[A <: ReactiveElement.Base] {
  def labels: Labels
  def root: A
}

object Component {
  given componentToModifierConversion[A <: ReactiveElement.Base]
      : Conversion[Component[A], Modifier[ReactiveElement.Base]] with {
    def apply(component: Component[A]): Modifier[ReactiveElement.Base] =
      component.root
  }
}
