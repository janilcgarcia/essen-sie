package essensie

import java.text.DecimalFormat

import scala.util.Random

import com.raquo.laminar.api.L
import com.raquo.laminar.api.L._
import org.scalajs.dom
import scala.scalajs.js
import dom.html
import com.raquo.laminar.nodes.{ReactiveElement, ReactiveHtmlElement}
import scala.scalajs.js.annotation.JSImport
import essensie.apimodels.Ingredient
import essensie.apimodels.MainSection

import essensie.components.EditableText
import essensie.i18n.Labels

import scala.concurrent.ExecutionContext.Implicits.global
import js.Thenable.Implicits.thenable2future
import io.taig.babel.Decoder
import io.taig.babel.circe.parser
import scala.concurrent.Future
import scala.util.Failure
import scala.util.Success

import io.circe.parser.decode
import essensie.apimodels
import apimodels.UserType
import java.net.CacheRequest
import io.circe.Codec
import com.raquo.waypoint.*
import io.circe.Encoder
import essensie.apimodels.RecipeDetails
import essensie.pages.RecipeEditor
import io.laminext.fetch.Fetch
import io.laminext.fetch.circe.*

sealed trait RequestError
case class HttpError(val status: Int) extends RequestError

sealed trait PageInfo
sealed trait ProtectedPageInfo extends PageInfo

case object LoginPageInfo extends PageInfo
case class RedirectPageInfo(val to: PageInfo) extends PageInfo

case object RecipesPageInfo extends ProtectedPageInfo
case object FallbackPageInfo extends ProtectedPageInfo
case class NewRecipePageInfo(val collection: Option[Long])
    extends ProtectedPageInfo
case class RecipeEditorPageInfo(val id: Long) extends ProtectedPageInfo

object PageInfo {
  given pageInfoCodec: Codec[PageInfo] = io.circe.generic.semiauto.deriveCodec
}

class Routes(val labels: Labels, val activeUser: Var[Option[UserType]]) {
  val loginRoute = Route.static(LoginPageInfo, root / "login" / endOfSegments)
  val recipesRoute = Route.static(RecipesPageInfo, root / endOfSegments)

  val recipeEditorRoute = Route(
    encode = (_: RecipeEditorPageInfo).id,
    decode = RecipeEditorPageInfo.apply,
    pattern = root / "recipes" / segment[Long] / endOfSegments
  )

  val newRecipeForCollectionEditorRoute = Route(
    encode = (_: NewRecipePageInfo).collection.get,
    decode = id => NewRecipePageInfo(Some(id)),
    pattern =
      root / "collections" / segment[Long] / "new-recipe" / endOfSegments
  )

  val newStandaloneRecipeEditorRoute = Route.static(
    NewRecipePageInfo(None),
    root / "recipes" / "new-recipe" / endOfSegments
  )

  val router = new Router[PageInfo](
    routes = List(
      loginRoute,
      recipesRoute,
      newStandaloneRecipeEditorRoute,
      newRecipeForCollectionEditorRoute,
      recipeEditorRoute
    ),
    getPageTitle = _ => "Breadpan",
    serializePage = page => Encoder[PageInfo].apply(page).noSpaces,
    deserializePage = s => decode[PageInfo](s).toTry.get,
    routeFallback = _ => FallbackPageInfo
  )(
    $popStateEvent = windowEvents.onPopState,
    owner = L.unsafeWindowOwner
  )

  val currentPage = router.$currentPage.combineWithFn(activeUser) {
    case (_: ProtectedPageInfo, None) =>
        RedirectPageInfo(LoginPageInfo)

    case (page, _) => page
  }

  private def navbarWrapper(container: Div) = div(
    nav(
      cls := ("navbar", "navbar-expand-lg", "navbar-light", "bg-light"),
      div(
        cls := "container-fluid",
        a(
          href := "/",
          cls := "navbar-brand",
          "Breadpan"
        )
      )
    ),

    container
  )

  val splitter = SplitRender[PageInfo, HtmlElement](currentPage)
    .collectStatic(LoginPageInfo)(pages.Login(labels, activeUser, router).root)
    .collectStatic(RecipesPageInfo)(navbarWrapper(
      pages.Recipes(labels, activeUser.signal.map(_.get), router).root
    ))
    .collectStatic(FallbackPageInfo) {
      div("Fallback sorry")
    }
    .collectSignal[RecipeEditorPageInfo] { pageSignal =>
      val initialRecipe = pageSignal.flatMap(page =>
        Fetch.get(s"/api/recipes/${page.id}").decodeOkay[RecipeDetails].data
      ).startWith(RecipeDetails(None, None, "", 500.0, MainSection("", Seq()), Seq()))

      navbarWrapper(RecipeEditor(labels, initialRecipe, router).root)
    }
    .collectSignal[NewRecipePageInfo] { page =>

      val initialRecipe = page.map(page =>
        RecipeDetails(
          None,
          page.collection,
          "New Recipe",
          500.0,
          MainSection("Main Section", Seq()),
          Seq()
        )
      )

      navbarWrapper(RecipeEditor(labels, initialRecipe, router).root)
    }
    .collectSignal[RedirectPageInfo] { page =>
      div(
        "Redirecting...",
        page.map(_.to) --> { nextState =>
          // If you call pushState before the element is mounted, it doesn't properly
          // redirect, apparently. This ensures pushState will only be called after the
          // route is mounted
          dom.window.setTimeout(
            () => router.pushState(nextState),
            0
          )
        }
      )
    }

  val view = splitter.$view
}

object Main {
  private def checkForUser(): Future[Either[RequestError, UserType]] =
    dom
      .fetch("/api/users/active")
      .flatMap { response =>
        if (response.ok)
          response
            .text()
            .map(decode[UserType])
            .transform(_.flatMap(_.toTry))
            .map(Right.apply)
        else
          Future.successful(Left(HttpError(response.status)))
      }

  private def fetchLabels(): Future[Labels] =
    dom
      .fetch("/i18n.json")
      .flatMap(_.text())
      .map(parser.parse)
      .transform(_.flatMap(_.toTry))
      .map(Decoder[Labels].decode)
      .transform(_.flatMap(_.toTry))

  private def setupPage(labels: Labels, activeUser: Var[Option[UserType]]): Unit = {
    val router = Routes(labels, activeUser)

    val container = dom.document.getElementById("app-container")

    render(
      container,
      div(
        child <-- router.view
      )
    )
  }

  def main(args: Array[String]): Unit = {
    // These objects need to be "called" so that the linker will know these modules must
    // be imported in JS
    Bootstrap
    BootstrapCSS

    fetchLabels().zip(checkForUser()).onComplete {
      case Success((labels, user)) =>
        setupPage(labels, Var(user.toOption))

      case Failure(ex) => ex.printStackTrace()
    }
  }
}
